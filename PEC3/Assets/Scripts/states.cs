﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class states : MonoBehaviour
{
    public gamecontroller gc;

    public objmove move;
    public GameObject bala;
    public float life = 100;
    public Text txt;
    public bool disparado;
    public bool instantiated = false;

    void Start(){

        disparado = false;

        move.enabled = true;

        txt.text = life+"";
        gc = GameObject.Find("GameManager").GetComponent<gamecontroller>();

    }

    void Update(){

        if(Input.GetButtonDown("Fire2")){
            move.anim.SetBool("isRunning",false);
            move.m_Rigidbody2D.velocity = Vector2.zero;
            move.enabled = false;

            if(move.m_FacingRight == true && instantiated == false){

                instantiated = true;

                GameObject g = Instantiate(bala, new Vector3(transform.position.x + 0.9f, transform.position.y + 0.9f, transform.position.z + 0.9f), Quaternion.identity);
                g.GetComponent<controldisparo>().m_FacingRight = true;

            }else if(instantiated == false){

                instantiated = true;
                
                GameObject g = Instantiate(bala, new Vector3(transform.position.x - 0.9f, transform.position.y + 0.9f, transform.position.z + 0.9f), Quaternion.Euler(0f, 0f, 180f));

                // Vector3 canv = g.transform.localScale;
                // canv.x *= -1;
                // g.transform.localScale = canv;

                g.GetComponent<controldisparo>().m_FacingRight = false;

            }

        }
        if(Input.GetButtonDown("Fire3")){
            move.enabled = true;
        }

        if(Input.GetButtonUp("Fire1"))
        {
            instantiated = false;
            disparado = true;            
            StartCoroutine(waiteame());

        }
    }


    public IEnumerator waiteame() 
    {
        print("esperando");
        yield return new WaitForSeconds(1);
        print("esperado"); 
        gc.changeturn();

        // print(Time.time - startTime);

    }


    public void Updatelife(float n){

        life -= n;
        txt.text = life+"";

    }

}
