﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class controldisparo : MonoBehaviour
{
    public gamecontroller gc;
    public cameracontroller cm;
    public LayerMask platforms;
    public GameObject enemigo;
    //////////////////////esta variable indica en que direccion se mueve el personaje, -1 izquierda +1 derecha
	private Rigidbody2D m_Rigidbody2D;
	public bool m_FacingRight = true;
    public SpriteRenderer sp;
    private bool firstseen = false;
    private bool groounded = true;
    public Camera mc;
    public float rotationTarget;
    private GameObject gm;
    public GameObject tilemapGameObject;
    public Tilemap tilemap;
    private float startTime;
    public float weaponRotationSpeed = 50f;
    float rot = 0;
    float y = 0;
    private states states;
    private float dañoDisparo = 2;

    // Start is called before the first frame update
    void Start()
    {

        tilemapGameObject = GameObject.Find("Tilemap");
        gm = gameObject;
        m_Rigidbody2D = GetComponent<Rigidbody2D>();
        tilemap = tilemapGameObject.GetComponent<Tilemap>();
    }
 
    // Update is called once per frame
    void Update()
    {
        
        RotateWeapon(Input.GetAxis("Vertical"));

        if(Input.GetButtonDown("Fire1")){

            // cm.obj = gameObject;
            startTime = Time.time;
            print(startTime+" start");

        }

        if(Input.GetButtonUp("Fire1"))
        {
            m_Rigidbody2D.simulated = true;
            y = rotationTarget;
            Vector2 dir = (Vector2)(Quaternion.Euler(0,0,y) * Vector2.right);
            // if(gameObject.GetComponent<objmove>().m_FacingRight == false){
            //     dir = (Vector2)(Quaternion.Euler(0,0,y) * Vector2.left);
            // }
            m_Rigidbody2D.AddForce(dir * ((Time.time - startTime) * 5), ForceMode2D.Impulse);
            startTime = 0;

        }
    }

     void RotateWeapon(float axis){
         rotationTarget = rot - axis;
         rot = Mathf.MoveTowardsAngle(rot, rotationTarget, Time.deltaTime * weaponRotationSpeed);
         transform.rotation = Quaternion.Euler(0f,0f,rot);
     }

    void OnCollisionEnter2D(Collision2D collision)
    {
        Vector3 hitPosition = Vector3.zero;
        if (tilemap != null && tilemapGameObject == collision.gameObject)
        {
            foreach (ContactPoint2D hit in collision.contacts)
            {
                hitPosition.x = hit.point.x - 0.01f * hit.normal.x;
                hitPosition.y = hit.point.y - 0.01f * hit.normal.y;
                tilemap.SetTile(tilemap.WorldToCell(hitPosition), null);
            }
        }else if(collision.gameObject.tag == "Player"){

            states = collision.gameObject.GetComponent<states>();

            float daño = (dañoDisparo * (Time.time - startTime));

            states.Updatelife(daño);

            collision.gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;


        }
        Destroy(gameObject);
        
    }

}
