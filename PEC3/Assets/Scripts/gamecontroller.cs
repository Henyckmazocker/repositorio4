﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class gamecontroller : MonoBehaviour
{

    public bool turno = true;
    public GameObject jugador;
    public GameObject enemigo;

    public cameracontroller cm;

    // Start is called before the first frame update
    void Start()
    {

        jugador.GetComponent<states>().enabled = true;

    }

    // Update is called once per frame
    void Update()
    {
        
        if(jugador.GetComponent<states>().life <= 0){
            SceneManager.LoadScene("endgame2");

        }

        if(enemigo.GetComponent<states>().life <= 0){
            SceneManager.LoadScene("endgame");

        }

    }


    public void changeturn(){
          if(jugador.GetComponent<states>().disparado == true){

            cm.obj = enemigo;

            enemigo.GetComponent<states>().enabled = true;
            enemigo.GetComponent<objmove>().enabled = true;
            enemigo.GetComponent<objmove>().firstseen = false;
            jugador.GetComponent<states>().enabled = false;
            jugador.GetComponent<states>().disparado = false;

        }else if(enemigo.GetComponent<states>().disparado == true){
            cm.obj = jugador;

            jugador.GetComponent<states>().enabled = true;
            jugador.GetComponent<objmove>().enabled = true;
            jugador.GetComponent<objmove>().firstseen = false;
            enemigo.GetComponent<states>().enabled = false;
            enemigo.GetComponent<states>().disparado = false;
        }
    }
}
