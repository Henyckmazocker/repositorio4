﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameracontroller : MonoBehaviour
{

    public GameObject obj;

    float coso;

    ////////////////////Función para que la camara persiga al personaje sobre el eje x

    private void FixedUpdate() {

        coso = transform.position.y;

        if(obj.transform.position.y > -10f){

            coso = obj.transform.position.y;

        }

        transform.position = new Vector3(obj.transform.position.x, coso,-1);

    }

}
