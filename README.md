PEC 3- Un juego de artilleria, David Carvajal Abellán.

Para esta actividad he optado por desarrollar un juego de artilleria tipo worms, pudiendo reutlizar ciertas funcionalidades de la practica anterior como el movimiento del personaje o el funcionamiento de los tilemaps.

El movimiento del personaje es prácticamente el mismo al de la práctica anterior, diferenciandose solo por que a la hora de disparar y durante el turno enemigo no podrás moverte y que ahora el personaje tiene animaciones, animaciones que distingo y controlo a través del codigo.

Para el disparo he creado un objeto que podremos rotar para indicar la parábola del disparo y midiendo cuanto tiempo se presiona el botón de disparar se indicará la fuerza del disparo.

El disparo podrá colisionar con el tilemap y debido a la detección individual de las tiles del mismo he podido hacer un escenario destruible.

Para agilizar la utilización de los GameObjects los he hecho a base de prefabs que facilitan mucho la reusabilidad de objetos en unity.

El disparo tiene un sistema de particulas generadas mediante el movimiento del objeto para que forme una especie de estela.

Para indicar tanto la vida de los personajes como las instrucciones del juego he usado canvas, en el caso de la vida son canvas hijo del objeto con el tag jugador para que se anclen a dicho objeto.
